#include <gtkmm.h>

class TestApp : public Gtk::Window {
public:
  TestApp();
};

TestApp::TestApp() {
  set_title("GTK C++ Test App");
  set_default_size(640, 480);
}

int main(int argc, char* argv[]) {
  auto app = Gtk::Application::create("com.example.gtkcpptest");
  return app->make_window_and_run<TestApp>(argc, argv);
}

